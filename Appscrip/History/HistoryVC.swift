//
//  HistoryVC.swift
//  Appscrip
//
//  Created by Manish on 17/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class HistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var tableHistory: UITableView!
    
    @IBOutlet weak var btnStartAain: UIButton!
    @IBOutlet weak var btnClearData: UIButton!
    var arrSummary = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        
        getDataFromDatabase()

        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        btnStartAain.layer.cornerRadius = btnStartAain.bounds.height/2
        btnStartAain.clipsToBounds = true
        btnStartAain.layer.borderColor = UIColor.lightGray.cgColor
        btnStartAain.layer.borderWidth = 1.5
        
        btnClearData.layer.cornerRadius = btnClearData.bounds.height/2
        btnClearData.clipsToBounds = true
        btnClearData.layer.borderColor = UIColor.lightGray.cgColor
        btnClearData.layer.borderWidth = 1.5
        
        tableHistory.estimatedRowHeight = 240
        tableHistory.rowHeight = UITableView.automaticDimension
        
        
    }
    
    func getDataFromDatabase() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Answer")
        let context = appDelegate.persistentContainer.viewContext
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            print("result is  \(result)")
            
            if result.count == 0 {
                
            }
            else {
                for data in result as! [NSManagedObject] {
                    print("data is  \(data)")
                    let keys = Array(data.entity.attributesByName.keys)
                    let dict = data.dictionaryWithValues(forKeys: keys)
                    print("dict is  \(dict)")
                    let swiftyJsonVar1 = JSON(dict)
                    print("swiftyJsonVar1 is  \(swiftyJsonVar1)")
                    arrSummary.append(swiftyJsonVar1)
                }
                tableHistory.reloadData()
            }
            
        } catch {
            print("Failed")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSummary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableHistory.dequeueReusableCell(withIdentifier: "cell") as! HistoryCell
        
        cell.lblGameCount.text = "Game \(indexPath.row + 1) : \(arrSummary[indexPath.row]["currentdate"].stringValue)"
        cell.lblName.text = "Name : \(arrSummary[indexPath.row]["name"].stringValue)"
        cell.lblAnswer2.text = "\(arrSummary[indexPath.row]["cricketer"].stringValue)"
        cell.lblAnswer3.text = "\(arrSummary[indexPath.row]["color"].stringValue)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UITableView.automaticDimension < 240 {
            return 240
        }
        else {
           return UITableView.automaticDimension
        }
    }
    
    
    
    @IBAction func actionStartAgain(_ sender: Any) {
        self.present(UIStoryboard.ques1, animated: true, completion: nil)
    }
    
    @IBAction func actionClearData(_ sender: Any) {
        clearData(entity: "Answer")
        self.present(UIStoryboard.ques1, animated: true, completion: nil)
    }
    
    func clearData(entity: String) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
            do {
                try managedContext.save()
            }
            catch {
                print(error)
            }
        } catch let error as NSError {
            print("Delete all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
