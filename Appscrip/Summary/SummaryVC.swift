//
//  SummaryVC.swift
//  Appscrip
//
//  Created by Manish on 17/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import CoreData

class SummaryVC: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAnswer2: UILabel!
    @IBOutlet weak var lblAnswer3: UILabel!
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var btnHistory: UIButton!
    
    var strDateTime = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        setUI()
    }
    
    func setUI() {
        btnFinish.layer.cornerRadius = btnFinish.bounds.height/2
        btnFinish.clipsToBounds = true
        btnFinish.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
        btnFinish.layer.borderWidth = 1.5
        
        btnHistory.layer.cornerRadius = btnHistory.bounds.height/2
        btnHistory.clipsToBounds = true
        btnHistory.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
        btnHistory.layer.borderWidth = 1.5
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        showData()
    }
    
    func showData() {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM,yyyy - HH:mm a"
        let selectedDate: String = formatter.string(from: date)
        strDateTime = selectedDate
        lblDateTime.text = "GAME : \(selectedDate)"
        lblName.text = "Hello : \(Global.answer1)"
        lblAnswer2.text = "Answer : \(Global.answer2)"
        lblAnswer3.text = "Answers : \(Global.answer3)"
    }
    
    @IBAction func actionFinish(_ sender: Any) {
        addToDatabase()
        self.present(UIStoryboard.ques1, animated: true, completion: nil)
    }
    
    func addToDatabase() {
        let context = appDelegate.persistentContainer.viewContext
        let refusal = Answer(context: context)
        refusal.name = Global.answer1
        refusal.cricketer = Global.answer2
        refusal.color = Global.answer3
        refusal.currentdate = strDateTime
        
        do {
            try context.save()
            
            Global.answer1 = ""
            Global.answer2 = ""
            Global.answer3 = ""
            
        } catch {
            print("Failed saving to local")
        }
    }
    
    @IBAction func actionHistory(_ sender: Any) {
        addToDatabase()
        self.present(UIStoryboard.history, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
