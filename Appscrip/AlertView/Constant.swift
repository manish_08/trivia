//
//  Constant.swift
//  Appscrip
//
//  Created by Manish on 17/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation

import UIKit

//MARK:- String Constants
enum StringConstants {
    
    enum Response {
        static var Success : String { return "Trivia" }
        static var Error : String { return "Trivia"}
    }
    
    enum Alert {
        
        enum Titles {
            static var Sorry : String { return "Trivia" }
            static var Success : String { return "Trivia" }
            static var AppName : String { return "Trivia" }
        }
        
        enum Messages {
            static var NoInternet : String { return "No Internet" }
            static var NoName : String { return "Please enter your name." }
             static var NoCricketer : String { return "Please select any one cricketer" }
             static var NoColor : String { return "Please select either one or more than one options." }

        }
        
        enum Actions {
            static var Okay : String { return "Okay" }
            static var Cancel : String { return "Cancel" }
        }
    }
}
