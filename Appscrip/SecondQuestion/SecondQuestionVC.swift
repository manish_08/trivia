//
//  SecondQuestionVC.swift
//  Appscrip
//
//  Created by Manish on 17/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class SecondQuestionVC: UIViewController {
    @IBOutlet weak var btnA: UIButton!
    @IBOutlet weak var btnB: UIButton!
    @IBOutlet weak var btnC: UIButton!
    @IBOutlet weak var btnD: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()

        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        btnA.layer.cornerRadius = 5.0
        btnA.clipsToBounds = true
        btnA.layer.borderColor = UIColor.lightGray.cgColor
        btnA.layer.borderWidth = 1.5
        
        btnB.layer.cornerRadius = 5.0
        btnB.clipsToBounds = true
        btnB.layer.borderColor = UIColor.lightGray.cgColor
        btnB.layer.borderWidth = 1.5
        
        btnC.layer.cornerRadius = 5.0
        btnC.clipsToBounds = true
        btnC.layer.borderColor = UIColor.lightGray.cgColor
        btnC.layer.borderWidth = 1.5
        
        btnD.layer.cornerRadius = 5.0
        btnD.clipsToBounds = true
        btnD.layer.borderColor = UIColor.lightGray.cgColor
        btnD.layer.borderWidth = 1.5
        
        btnNext.layer.cornerRadius = btnNext.bounds.height/2
        btnNext.clipsToBounds = true
        btnNext.layer.borderColor = UIColor.lightGray.cgColor
        btnNext.layer.borderWidth = 1.5
        
    }
    
    @IBAction func actionOption(_ sender: UIButton) {
        if sender == btnA {
            selectA()
        }
        if sender == btnB {
            selectB()
        }
        if sender == btnC {
            selectC()
        }
        if sender == btnD {
            selectD()
        }
        Global.answer2 = sender.titleLabel!.text!
    }
    
    func selectA() {
        btnA.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        btnB.backgroundColor = UIColor.clear
        btnC.backgroundColor = UIColor.clear
        btnD.backgroundColor = UIColor.clear
    }
    
    func selectB() {
        btnB.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        btnA.backgroundColor = UIColor.clear
        btnC.backgroundColor = UIColor.clear
        btnD.backgroundColor = UIColor.clear
    }
    
    func selectC() {
        btnC.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        btnB.backgroundColor = UIColor.clear
        btnA.backgroundColor = UIColor.clear
        btnD.backgroundColor = UIColor.clear
    }
    
    func selectD() {
        btnD.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        btnB.backgroundColor = UIColor.clear
        btnC.backgroundColor = UIColor.clear
        btnA.backgroundColor = UIColor.clear
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if Global.answer2 == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.AppName, message: StringConstants.Alert.Messages.NoCricketer, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            self.present(UIStoryboard.ques3, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
