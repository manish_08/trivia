//
//  ViewController.swift
//  Appscrip
//
//  Created by Manish on 17/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class ViewController: UIViewController {
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUI()
        
    }
    func setUI() {
        btnNext.layer.cornerRadius = btnNext.bounds.height/2
        btnNext.clipsToBounds = true
        btnNext.layer.borderColor = UIColor.lightGray.cgColor
        btnNext.layer.borderWidth = 1.5
    }

    @IBAction func actionNext(_ sender: Any) {
        self.view.endEditing(true)
        if checkvalidation() {
            Global.answer1 = txtName.text!
            self.present(UIStoryboard.ques2, animated: true, completion: nil)
        }
    }
    
    func checkvalidation() -> Bool {
        if txtName.text == "" {
            UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.AppName, message: StringConstants.Alert.Messages.NoName, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
            return false
        }
        return true
    }
    
    
}

