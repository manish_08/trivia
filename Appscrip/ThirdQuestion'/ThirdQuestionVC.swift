//
//  ThirdQuestionVC.swift
//  Appscrip
//
//  Created by Manish on 17/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON


class ThirdQuestionVC: UIViewController {
    @IBOutlet weak var btnA: UIButton!
    @IBOutlet weak var btnB: UIButton!
    @IBOutlet weak var btnC: UIButton!
    @IBOutlet weak var btnD: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var arrSelection = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        btnA.layer.cornerRadius = 5.0
        btnA.clipsToBounds = true
        btnA.layer.borderColor = UIColor.lightGray.cgColor
        btnA.layer.borderWidth = 1.5
        
        btnB.layer.cornerRadius = 5.0
        btnB.clipsToBounds = true
        btnB.layer.borderColor = UIColor.lightGray.cgColor
        btnB.layer.borderWidth = 1.5
        
        btnC.layer.cornerRadius = 5.0
        btnC.clipsToBounds = true
        btnC.layer.borderColor = UIColor.lightGray.cgColor
        btnC.layer.borderWidth = 1.5
        
        btnD.layer.cornerRadius = 5.0
        btnD.clipsToBounds = true
        btnD.layer.borderColor = UIColor.lightGray.cgColor
        btnD.layer.borderWidth = 1.5
        
        btnNext.layer.cornerRadius = btnNext.bounds.height/2
        btnNext.clipsToBounds = true
        btnNext.layer.borderColor = UIColor.lightGray.cgColor
        btnNext.layer.borderWidth = 1.5
        
    }
    
    
    @IBAction func sctionSelection(_ sender: UIButton) {
        if arrSelection.contains(sender.titleLabel!.text!) {
            for index in 0..<arrSelection.count {
                if arrSelection[index] == sender.titleLabel!.text! {
                    arrSelection.remove(at: index)
                    sender.backgroundColor = UIColor.clear
                    break
                }
            }
        }
        else {
            arrSelection.append(sender.titleLabel!.text!)
            sender.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        }
    }

    
    @IBAction func actionNext(_ sender: Any) {
        if arrSelection.count == 0 {
             UIAlertController.showAlert(withTitle: StringConstants.Alert.Titles.AppName, message: StringConstants.Alert.Messages.NoColor, style: .alert, buttons: [AlertAction(text: StringConstants.Alert.Actions.Okay)])
        }
        else {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM,yyyy-HH:mm a"
            let selectedDate: String = formatter.string(from: date)
            let controller = UIStoryboard.summary
            controller.strDateTime = selectedDate
            Global.answer3 = arrSelection.joined(separator: ", ")
            self.present(UIStoryboard.summary, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
