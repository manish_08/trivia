//
//  StoryBoard.swift
//  Appscrip
//
//  Created by Manish on 17/10/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    private class var main : UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class var ques1 : ViewController {
        return self.main.instantiateViewController(withIdentifier: String(describing: ViewController.self)) as! ViewController
    }
    
    class var ques2 : SecondQuestionVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: SecondQuestionVC.self)) as! SecondQuestionVC
    }
    
    class var ques3 : ThirdQuestionVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: ThirdQuestionVC.self)) as! ThirdQuestionVC
    }
    
    class var summary : SummaryVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: SummaryVC.self)) as! SummaryVC
    }
    
    class var history : HistoryVC {
        return self.main.instantiateViewController(withIdentifier: String(describing: HistoryVC.self)) as! HistoryVC
    }
    
   
    
    
   
    
    
    
    
}
